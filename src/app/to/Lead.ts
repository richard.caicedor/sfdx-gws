export class Lead{

	constructor(
		public Name:string,
		public Company:string,
		public Email:string,
		public Industry:string,
		public Status:string,
		public Phone:string,
		public Title:string,
		public City:string
	){}
} 