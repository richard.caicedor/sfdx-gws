import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions , URLSearchParams } from '@angular/http';
import { map } from 'rxjs/operators';
import { constant } from './constants';
import * as $ from 'jquery';

@Injectable()
export class Servicio {
  
  public access_token:string; 

  constructor(public _http:Http) {
    this.access_token = localStorage.getItem('access_token');
  }

  getItems(method:string) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json'); 
    headers.append('Authorization', 'Authorization: Bearer '+this.access_token);
    let options = new RequestOptions({ headers: headers });
    return this._http.get(constant.URL.API+method, options).pipe(map(res => res.json()));
  } 

  postCar(method:string,data:any) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json'); 
    headers.append('Authorization', 'Authorization: Bearer '+this.access_token);
    let options = new RequestOptions({ headers: headers });  
    return this._http.post(constant.URL.API+method, JSON.stringify(data) , options).pipe(map(res => res.json()));
  } 


  getToken(){
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let urlSearchParams = new URLSearchParams();
    urlSearchParams.append('username', constant.DATATOKEN.USERNAME);
    urlSearchParams.append('password', constant.DATATOKEN.PASSWORD);
    urlSearchParams.append('grant_type', constant.DATATOKEN.GRANT_TYPE);
    urlSearchParams.append('client_id', constant.DATATOKEN.CLIENT_ID);
    urlSearchParams.append('client_secret', constant.DATATOKEN.CLIENT_SERCRET);
    let options = new RequestOptions({ headers: headers }); 
    return this._http.post(constant.URL.TOKEN, urlSearchParams, options).pipe(map(res => res.json()));
  }



/*
  getItems(id: string, urlApi: string) {
    $('.loaderBox').show();
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    let options = new RequestOptions({ headers: headers });
    let urlConsulta = '';
    if (id != '0') {
      urlConsulta = this.url + urlApi + '/' + id;
    } else {
      urlConsulta = this.url + urlApi;
    }
    return this._http.get(urlConsulta, options).pipe(map(res => res.json()));
  }

  addItem(objeto: any, urlApi: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post(this.url + urlApi, objeto, options).pipe(map(res => res.json()));
  }

  editItem(objeto: any, urlApi: string) {
    let headers = new Headers();  
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post(this.url + urlApi, objeto, options).pipe(map(res => res.json()));
  }

  loginOn(objeto: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this._http.post(this.url + 'login', objeto, options).pipe(
      map(res => {
        let token = res.json().token;
        localStorage.setItem('user', token);

        return token != false;
      })
    );
  }
  UploadFile(ruta: string, files, valores) {
    var formData: any = new FormData();
    formData.append('file', files[0]);
    formData.append('fecha_inicio', valores.fecha_inicio);
    formData.append('fecha_fin', valores.fecha_fin);
    formData.append('page_id', valores.page_id);
    this._http.post(this.url + ruta, formData).subscribe(res => {
      console.log(res);
    });
  } */
}
