import { Component, OnInit } from '@angular/core';
import { Servicio } from '../services/servicio';
import { Lead } from '../to/Lead';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  providers: [Servicio]
})
export class CarComponent implements OnInit {
   
   public Leads:any;
   public idLead:string;
   public Lead:Lead;
   public Cars:any;
   public CarForm:FormGroup;

  constructor(public _servicio:Servicio) { 
    this.CarForm = this.createFormGroup();
    this.idLead =''; 
  }

  createFormGroup(){
    return new FormGroup({
      Name: new FormControl('',[Validators.required]),
      Model_Year__c: new FormControl(''),
      Price__c: new FormControl('',[Validators.required]),
      Tax__c: new FormControl(''),
      Registration_Plate__c: new FormControl('',[Validators.required]),
      Lead__c: new FormControl(''),
      Leasing__c: new FormControl(''),
    });  
  } 

  ngOnInit() {
    this.getListas();
  }
 
  onSaveForm(){
    if(!this.CarForm.invalid){ 
      this.CarForm.controls['Lead__c'].setValue(this.idLead); 
      let data = { 'objCar': this.CarForm.value } 
        this._servicio.postCar('Cars',data).subscribe(
        result => {
          alert('Registro Creado Correctamente!')
          this.CarForm.reset();
        },
        error => {
          console.log(<any>error);
        }
      );
    }else{
      alert('Por favor diligenciar Datos Obligatorios')
    }
  }

  OngetCarsList(){
    this.Lead = this.Leads.find( lead => lead.Id === this.idLead );
    this._servicio.getItems('Cars?idLead='+this.idLead).subscribe(
      result => {
        this.Cars = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  getListas(){
    this._servicio.getItems('Leads').subscribe(
      result => {
        this.Leads = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
