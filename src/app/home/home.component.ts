import { Component } from '@angular/core';

@Component({
  selector: 'home-root',
  templateUrl: './home.component.html',
})
export class HomeComponent {

	public message:string;
	public style:string;

  constructor(){
  }

  ngOnInit() {
  	this.message = 'Token Generado Correctamento!';
  	this.style = 'alert-success';
  }
  

}
