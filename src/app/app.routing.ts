import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Componentes
import { CarComponent } from './car/car.component';
import { HomeComponent } from './home/home.component';
import { LeadComponent } from './lead/lead.component';


const appRoutes: Routes = [
	{path: '', component: HomeComponent },
	{path: 'cars', component: CarComponent},
	{path: 'leads', component: LeadComponent},
];

export const appRoutingProviders:any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
  