import { Component } from '@angular/core';
import { Servicio } from './services/servicio';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Servicio]
})
export class AppComponent {

  title = 'Test Global Web Solution';
  public access_token:string;

  constructor(public _servicio:Servicio){
  	this.access_token = localStorage.getItem('access_token');
  }

  GenerarToken(){
  	this._servicio.getToken().subscribe(
      result => {
        localStorage.setItem('access_token', result.access_token);
        this.access_token = 'true';
      },
      error => {
        console.log(<any>error);
      }
    );;
  }
}
