import { Component, OnInit } from '@angular/core';
import { Servicio } from '../services/servicio';
import { Lead } from '../to/Lead';

@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  providers: [Servicio]
})
export class LeadComponent implements OnInit {
   
   public Leads:any;
   public idLead:string;
   public Lead:Lead;
   public Cars:any;

  constructor(public _servicio:Servicio) { 
    this.idLead ='';
  }

  ngOnInit() {
  	this.getListas();
  }

  OngetCarsList(){
    this.Lead = this.Leads.find( lead => lead.Id === this.idLead );
    this._servicio.getItems('Cars?idLead='+this.idLead).subscribe(
      result => {
        this.Cars = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  getListas(){
    this._servicio.getItems('Leads').subscribe(
      result => {
        this.Leads = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
